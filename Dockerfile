FROM openjdk:8-jdk-alpine AS BUILD
COPY . .
RUN ./mvnw package

FROM openjdk:8-jdk-alpine
WORKDIR /app
COPY --from=BUILD /target/*.jar /app/app.jar
ENTRYPOINT ["java", "-jar", "app.jar"]